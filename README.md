# EARLY WORK - NOT YET USABLE

<div align="center">
<p align="center"><img src="https://gitlab.com/troll-engine/platform-mac/raw/master/logo.svg" align="center" width="350" height="250" alt="Project icon"></p>

<blockquote>
<p align="center">The Mac platform project for Troll Engine</p>
</blockquote>

<p align="center">
  <a href="https://gitlab.com/troll-engine/platform-mac/commits/master" alt="pipeline status"><img src="https://gitlab.com/troll-engine/platform-mac/badges/master/pipeline.svg" /></a>
  <a href="https://gitlab.com/troll-engine/platform-mac/commits/master" alt="coverage report"><img src="https://gitlab.com/troll-engine/platform-mac/badges/master/coverage.svg" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/platform-mac" alt="version"><img src="https://badgen.net/npm/v/@troll-engine/platform-mac" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/platform-mac" alt="license"><img src="https://badgen.net/npm/license/@troll-engine/platform-mac" /></a>
  <a href="https://www.npmjs.com/package/@troll-engine/platform-mac" alt="downloads"><img src="https://badgen.net/npm/dt/@troll-engine/platform-mac" /></a>
  <a href="https://discord.gg/ewwaSxv" alt="discord"><img src="https://discordapp.com/api/guilds/621750483098271745/widget.png" /></a>
</p>

</div>
</p>

```bash
npm install --save @troll-engine/platform-mac
```
